const express = require('express');
const router = express.Router();
const db = require("../database");


router.get('/sign-up', (req, res) => {
    res.sendFile('signup.html');
});

//pentru adaugarea unei noi notite din pagina myNotes
router.post('/myNotes', async(req, res) => {
    const { subject_name, description } = req.body;
    const newNote = {
        subject_name,
        description
    };
    
    await pool.query('INSERT INTO notes set ?', [newNote]);
    res.send('recieved');
})

router.get('/myNotes', async (req, res) => {
    const notes = await pool.query('SELECT * FROM notes');
    console.log(notes);
});


//stergerea unei notite
router.get('/delete/:id', async(req,res) => {
    const { id } = req.params;
    pool.query('DELETE FROM notes WHERE ID = ?', [id]);
    res.redirect('/myNotes.html');
})

//editarea unei notite
router.get('/edit/:id', async(req, res) => {
    const { id } = req.params;
    const { subject_name, description} = req.body;
    const newNote = {
        subject_name,
        description
    };
    
    await pool.query('UPDATE notes set ? WHERE id =?', [newNote, id]);
    res.redirect('/myNotes.html');
})

module.exports = router;