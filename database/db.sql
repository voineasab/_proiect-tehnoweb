--trebuie folosita comanda: root mysql -u root -p pentru a va conecta la mysql
--daca folositi doar mysql -u root -p, nu o sa functioneze :(
--parola = admin

CREATE DATABASE database_notes;


USE database_notes;

--users table
CREATE TABLE users(
    id INT(11) NOT NULL,
    stud_ase_email VARCHAR(16) NOT NULL, 
    password VARCHAR(20) NOT NULL,
    name VARCHAR(16) NOT NULL, 
    surname VARCHAR(16) NOT NULL
);

ALTER TABLE users
    ADD PRIMARY KEY (id);

ALTER TABLE users
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

DESCRIBE users;

-- notes table
CREATE TABLE notes (
    id INT(11) NOT NULL,
    subject_name VARCHAR(150) NOT NULL,
    description TEXT,
    user_id INT(11),
    CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES users(id)
);

ALTER TABLE notes
    ADD PRIMARY KEY (id);
    
ALTER TABLE notes
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
    
DESCRIBE notes;