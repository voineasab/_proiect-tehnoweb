const express = require("express");
const app = express();
const path = require('path');


//settings
app.set('port', process.env.PORT || 3000);
app.set('ip', process.env.IP);


//middlewares
app.use(express.json());

//global variables
app.use((req, res, next) => {
    next();
})


//routes
app.use(require('./src/routes'));
app.use(require('./src/routes/authentication'));
app.use('/notes', require('./src/routes/notes'));



//listen
app.listen(process.env.PORT, process.env.IP, function(){
    console.log("Server on port:" + app.get('port'));
});